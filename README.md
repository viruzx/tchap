#TchaP -- A Transparent Chat Protocol

#Abandonned Project
During the course of this project, I learned a lot... and I mean A LOT and not sure who to thank for that. Sadly however, this project is going to end a up a massive failure if I continue.
This project was intended to be used for massive use but it seems that it would fail at that since a single TCP stream doesn't go both ways in the same time and can't be written to from 2 different sources.

The concept behind this project is decent, I'll get myself that amount of credit. The implementatin however, is a failure. The code is good, it's the logic behind it that was flawed and I realised it too late into the project. So late that I just decided to give up on this as trying to fix it would be rewriting  the entire project.

I tried. I failed. I will try again.

#Important!!!
Do not install this server "as is". A private and public keypair are stored in this repository, please overwrite these when using in production as it isn't secure.

##The Standard
TchaP is currenly following standard draft v0.12 you may review [here](https://bitbucket.org/viruzx/tchap/src/HEAD/standard.md).

##Our Mission
Our mission is to create an open-source communication protcol that is theoretically unbreakable. There project is something between [Tox](https://tox.chat) and the well known IRC protocol. The idea is instead of strict protocols, TchaP is a flexible one thanks to the servers' oblivious nature about the data that they relay - they just relay it.

###*We want to provide a secure messaging platform using state-of-the-art technologies and then push the limits of that idea.*

#The server
The current server uses NodeJS but it can be rebuilt to work in nearly any modern programming language. Once we hit the official stable release, we will start to work on a Java server.

#The client
As of now, there is no official functioning client. In this repository, `client.js` is a PoC client. It only communicates with itself through a designated server.

##FAQ

###How secure is this?
Theoretically unbreakable encryption on par with SSL but minus vulnerabilities such as anything that ressembles heartbleed by not being more complicated than it has to be.

###How large is the team working on this?
One and in need of help.

###How can I get involved?/How can I help out?
First of all, donations are always welcome -- we take paypal at slava@knyz.org. Next if you are developer, we need help on building clients for this. Care about the future of this project but no time to invest? Subscribe the our soon-to-come mailing list so we can discuss key features. You are also welcome to promote us on social media and blogs so that other people can get interested in helping out.

###What's the original purpose?
None. A project born out of interest.

###Where can I find the Docs?
Doesn't exist. The close as it gets is the standard draft which you may find [here](https://bitbucket.org/viruzx/tchap/src/HEAD/standard.md).

###Why are you making up these questions?
I don't know.
