#TchaP Standard Draft 0.12

##Introduction

This document will attempt lay out the current standard for TchaP communication as of version 0.11 which **will** be subject to change.

#1. Basic concept

##1.1. The Name

TchaP comes from playing around with the acronym TCP and the word “Chat” since all communication is meant to relay via TCP, which gave TChatP. TChatP ended up being nearly impossible to pronounce so we dropped the “t” while still keeping the TCP acronym giving us with Tchap (pronounced either as Tea Chap, Tea Cha Pea or Tchap in a single syllable).

##1.2. Objective

The objective of this communication protocole is to provide a secure, discentrilised way of communicating with peers. This concept is heavily inspired by Tox and brings the same challenges such as offline messaging.

##1.3. Role of the server.

The server’s role is to act as a middleman between all clients allowing group chats and compatibility with TOR (this can be ran on as a hidden service). The server’s sole purpose is to forward all incoming data to appropriate sockets by decrypting incoming data and re-encrypting it again.

##1.4. Encryption

A TchaP server **must** be secure to even function. The proceedure to secure the server is a simplified version of HTTPS. Upon connecting, the client is responsible to provide an encryption key that it wants to use which **must** sent encrypted by the public key. The client is responsible for encrypting all data sent to the server and the server has the responsability for encrypting all data leaving the server.

#2. Metadata Standard

##2.1. Initial connection
A connection to the server triggers your temporary registration which consists of getting the salted MD5 hash of your remote IP and remote port (There is no effective way of finding this information out).

##2.2. First communication
The first communication sets the AES encryption key for all future communications and is the sole time JSON is not used, saving on the amount of bytes on this exchange. The first data sent to the server must be an RSA-signed short string of a length permitted by the public encryption key (32 is a safe bet).

##2.3. Next communications
All communications **after** the first communication need to be encrypted with the AES key.

##2.4. Incoming connections
All incoming connections will be encrypted with the AES key that you set.

*The AES key should be generated randomly for each connection. There is no system in place to enforce this and never will be. Your data is only as secure as you want it to be.*

#3. Communication Standard

*Note: As pointed out in **2.2**, the first communication is exempt from this standard.*

##3.1. All Requests
All requests to the server are composed of 2 main parts:

 - **META**: Read and processed by server
 - **DATA**: Either arbitrary data to be forwarded or data to be processed by the server (depending on the *META*)

As of draft version 0.11, the only information required in the **META** section is the `FWD` object. It can contain one of 3 values, either `false` to mark it as to be processed internally, `true` if it is a public broadcast (In the talks, not in active builds) or a list of clients to be forwarded to.

##3.2. Forward Requests
Forward requests are the most frequent ones, they are the ones that tell the server what data to forward to other clients. A sample (decrypted) request would look like this:
```
{
 "META":{
  "FWD": ["client_id1", "client_id2"]
 },
 "DATA": {
  "message": "Blah blah blah"
 }
}
```

The system will broadcast the **DATA** section untampered to all clients listed in the `FWD` bracket and will callback for each successful/failed transmission.

Sample callback:

```
{
 "META": {
  "sender": "SYSTEM"
 },
 "DATA":{
  "code": "0",
  "message": "$client_id1 is disconnected"
 }
}
{
 "META": {
  "sender": "SYSTEM"
 },
 "DATA":{
  "code": "1",
  "message": "$client_id2 got message."
 }
}
```
##3.3. Internal Requests
*This section is vastly incomplete*

Internal requests are requests sent to the server that will not be forwarded but processed interally by the server in order to complete a desired function. This will be used to set *aliases*, request server data such as server time, address and so on.

Sample request:

```
{
 "META":{
  "FWD": false
 },
 "DATA": {
  "function": "SET_ALIAS",
  "alias_name": "Maintainer",
  "password": "pa$sw0rd123"
 }
}
```
