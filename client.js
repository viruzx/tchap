var readlineSync = require('readline-sync');
console.log("This is a PoC chat client using the TchaP transparent chat protocol. It can communicate with a single client.");

//Configuration
var server_host = "slava.chamandj.top",
    server_port = 3000,
    client_alias = readlineSync.question('Name: '),
    client_member = readlineSync.question('Partner: '),
    //client_alias = "Tester",
    AES_key = "test"; //Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 4);

var net = require('net');
var readlineSync = require('readline-sync');
var colors = require('colors');

var fs = require('fs'),
    ursa = require('ursa'),
    crt, key;

key = ursa.createPrivateKey(fs.readFileSync('key.pem'));
crt = ursa.createPublicKey(fs.readFileSync('key.pub'));
var crypto = require('crypto'),
    password = AES_key,
    algorithm = 'aes-256-ctr';

function rsa_encrypt(text) {
    return crt.encrypt(text, 'utf8', 'base64');
}

function rsa_decrypt(text) {
    return key.decrypt(text, 'base64', 'utf8');
}

function aes_encrypt(text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function aes_decrypt(text) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}


var host = server_host;
var port = server_port;
var name = null;
var client = new net.Socket();

client.connect(port, host, function() {
    console.log('Initiating handshake');
    //Send AES encryption key
    client.write(rsa_encrypt(password) + "$");
    console.log(rsa_encrypt(password) + "$");
});
var handshake = true;
function getAlias(alias){
    console.log("Requesting alias", alias);
    var request = {
        META: {
            FWD: false,
            REQUEST: "SET_ALIAS"
        },
        DATA: {
            alias: alias,
            password: "password"
        }
    }
    client.write(aes_encrypt(JSON.stringify(request)) + "$");
}
client.on('data', function(data) {
    //Convert raw encrypted packets to JSON Object
    var data = JSON.parse(aes_decrypt(data.toString()));
    //console.log(data);
    if (handshake) {
        console.log("[" + data.TIME + "]" + "Handshake completed.")
        console.log(data.DATA.SERVER.NAME + " welcomes " + data.DATA.CLIENT.NAME);
        name = data.DATA.CLIENT.NAME;
        sendmsg(data.DATA.CLIENT.NAME, "Connection works");
        sendmsg(data.DATA.CLIENT.NAME, "Connection STILL works");
        handshake = false;

        //getAlias() function is broken for some reason...

        //getAlias(client_alias);
        //name = client_alias;
    } else {
        console.log("[" + data.TIME + "]" + data.SENDER + ": " + data.DATA.message);
    }
});

client.on('close', function() {
    console.log('Connection closed');
    process.exit(1);
});

var stdin = process.openStdin();

function sendmsg(user, text) {
    var data = {
        META: {
            FWD: [user]
            //FWD: ["Potato", "Tester"]
        },
        DATA: {
            message: text
        }
    }
    client.write(aes_encrypt(JSON.stringify(data)) + "$");
    console.log(aes_encrypt(JSON.stringify(data)));
}

//Below this line is just the prompt. Don't worry about it ;)
var readline = require('readline'),
    util = require('util');

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    completer: completer
});
rl.setPrompt("> ", 2);
rl.on("line", function(line) {
    sendmsg(client_member, line);
    rl.prompt();
});
rl.on('close', function() {
    return process.exit(1);
});
rl.on("SIGINT", function() {
    rl.clearLine();
    rl.question("Confirm exit : ", function(answer) {
        return (answer.match(/^o(ui)?$/i) || answer.match(/^y(es)?$/i)) ? process.exit(1) : rl.output.write("> ");
    });
});
rl.prompt();

var fu = function(type, args) {
    var t = Math.ceil((rl.line.length + 3) / process.stdout.columns);
    var text = util.format.apply(console, args);
    rl.output.write("\n\x1B[" + t + "A\x1B[0J");
    rl.output.write(text + "\n");
    rl.output.write(Array(t).join("\n\x1B[E"));
    rl._refreshLine();
};

console.log = function() {
    fu("log", arguments);
};
console.warn = function() {
    fu("warn", arguments);
};
console.info = function() {
    fu("info", arguments);
};
console.error = function() {
    fu("error", arguments);
};

console.log(">> Interface Loaded.");

function completer(line) {
    var completions = ["test", "command1", "command2", "login", "check", "ping"];
    var hits = completions.filter(function(c) {
        return c.indexOf(line) == 0;
    });

    if (hits.length == 1) {
        return [hits, line];
    } else {
        console.log("Suggest :");
        var list = "",
            l = 0,
            c = "",
            t = hits.length ? hits : completions;
        for (var i = 0; i < t.length; i++) {
            c = t[i].replace(/(\s*)$/g, "")
            if (list != "") {
                list += ", ";
            }
            if (((list + c).length + 4 - l) > process.stdout.columns) {
                list += "\n";
                l = list.length;
            }
            list += c;
        }
        console.log(list + "\n");
        return [hits, line];
    }
}
